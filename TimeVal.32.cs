﻿//https://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html#index-struct-timeval
//https://stackoverflow.com/questions/6274878/what-is-c-time-t-equivalent-for-c-sharp/6274975
//https://raspberrypi.stackexchange.com/questions/12600/will-rpi-suffer-from-the-y2k38-bug

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public partial struct TimeVal
{
    public int tv_sec;
    public long tv_nsec;
}



