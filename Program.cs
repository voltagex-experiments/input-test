﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;



namespace InputTest
{


    //https://www.kernel.org/doc/html/v4.15/input/input.html

    [StructLayout(LayoutKind.Sequential)]
    struct InputEvent
    {
        public TimeVal time;
        public ushort type;
        public ushort code;
        public uint value;
    }


    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            string wantDebug = Environment.GetEnvironmentVariable("DEBUG");
            if (!string.IsNullOrEmpty(wantDebug) && int.TryParse(wantDebug, out int debug) && debug > 0)
            {
                Console.WriteLine("Waiting for remote debugger");
                while (!Debugger.IsAttached)
                {
                    Thread.Sleep(500);
                }
                Console.WriteLine("Debugger attached, breaking.");
                Debugger.Break();
            }
#endif
            var inputEvent = new InputEvent();
            byte[] buffer = new byte[16];
            Span<byte> bufferSpan = new Span<byte>(buffer);
            using (FileStream inputStream = new FileStream("/dev/input/event0", FileMode.Open, FileAccess.Read))
            {
               
                while (true)
                {
                   int bytesRead = inputStream.Read(bufferSpan);
                   if (bytesRead == 16)
                    {
                        //Timeval time - 2+2 bytes
                        inputEvent.time.tv_sec = BitConverter.ToInt32(buffer,0);
                        inputEvent.time.tv_nsec = BitConverter.ToInt32(buffer, 4);
                        inputEvent.type = BitConverter.ToUInt16(buffer, 8);
                        inputEvent.code = BitConverter.ToUInt16(buffer, 10);
                        inputEvent.value = BitConverter.ToUInt32(buffer, 12);
                        Console.WriteLine($"Sec: {inputEvent.time.tv_sec}, Nsec: {inputEvent.time.tv_nsec}, Type: {inputEvent.type}, Code: {inputEvent.code}, Value: {inputEvent.value}");
                        Console.WriteLine(BitConverter.ToString(buffer));
                    }

                }


            }



        }
    }
}


