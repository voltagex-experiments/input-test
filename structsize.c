#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

//https://www.kernel.org/doc/html/v4.15/input/input.html#event-interface
struct input_event {
        struct timeval time;
        unsigned short type;
        unsigned short code;
        unsigned int value;
};

int main(void)
{
    int sizeof_time_t = sizeof(time_t);
    int sizeof_timeval = sizeof(struct timeval);
    int sizeof_input_event = sizeof(struct input_event);

    printf("time_t: %u bytes\n", sizeof_time_t);
    printf("timeval: %u bytes\n", sizeof_timeval);
    printf("input_event: %u bytes\n", sizeof_input_event);
    exit(0);
}
